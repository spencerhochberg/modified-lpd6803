LPD6803 Library
===============

This is an inelegant modification of the LPD6803 library to make it control multiple strips. Instead of making more strip objects, I just added a function strip.defineData(int DataPin); Pass this the data pin number and the next strip.show() will update that pin. 